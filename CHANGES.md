#Changelog
##1.1.1
* Bump yin-pda-core to 1.1.2
##1.1.0
* Provide the estimation confidence values with results
##1.0.1
* Bump yin-pda-core to 1.0.1
* Provide HTML5 javadocs
* Correct the builds for JDK7 target on newer JDKs
##1.0.0
Initial release