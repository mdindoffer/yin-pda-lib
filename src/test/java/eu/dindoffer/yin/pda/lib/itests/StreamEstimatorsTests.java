package eu.dindoffer.yin.pda.lib.itests;

import eu.dindoffer.yin.pda.core.api.F0Candidate;
import eu.dindoffer.yin.pda.core.api.YinSettings;
import eu.dindoffer.yin.pda.lib.utils.YinBoundedStreamEstimator;
import eu.dindoffer.yin.pda.lib.utils.YinUnboundedStreamEstimator;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author Martin Dindoffer contact@dindoffer.eu
 */
public class StreamEstimatorsTests {

    private static final String ASC_440HZ_SINE_MONO_16B_44P1KHZ_REC = "/asc_440hz_sine_mono_16b_44-1khz.wav";
    private static final YinSettings SETTINGS = new YinSettings.Builder().build();

    @Test(timeOut = 60 * 1000)
    public void outputEqualityTest() throws UnsupportedAudioFileException, IOException, InterruptedException,
            ExecutionException {
        URL recording = getClass().getResource(ASC_440HZ_SINE_MONO_16B_44P1KHZ_REC);
        List<F0Candidate> eagerResults = doBoundedEagerEstimation(recording);
        List<F0Candidate> lazyResults = doBoundedLazyEstimation(recording);
        List<F0Candidate> unboundedResults = doUnboundedEstimation(recording);
        Assert.assertEquals(eagerResults, lazyResults);
        Assert.assertEquals(eagerResults, unboundedResults);
    }

    private List<F0Candidate> doBoundedEagerEstimation(URL recording) throws UnsupportedAudioFileException, IOException,
            InterruptedException, ExecutionException {
        try (AudioInputStream ais = AudioSystem.getAudioInputStream(recording)) {
            YinBoundedStreamEstimator yse = new YinBoundedStreamEstimator(ais, SETTINGS, 4);
            List<Future<F0Candidate>> estimates = yse.estimateAllFramesEagerly();
            List<F0Candidate> results = new ArrayList<>(estimates.size());
            for (Future<F0Candidate> f : estimates) {
                results.add(f.get());
            }
            return results;
        }
    }

    private List<F0Candidate> doBoundedLazyEstimation(URL recording) throws UnsupportedAudioFileException, IOException,
            InterruptedException, ExecutionException {
        try (AudioInputStream ais = AudioSystem.getAudioInputStream(recording)) {
            YinBoundedStreamEstimator yse = new YinBoundedStreamEstimator(ais, SETTINGS, 4);
            List<Future<F0Candidate>> estimates = yse.estimateAvailableDataLazily();
            List<F0Candidate> results = new ArrayList<>(estimates.size());
            for (Future<F0Candidate> f : estimates) {
                results.add(f.get());
            }
            return results;
        }
    }

    private List<F0Candidate> doUnboundedEstimation(URL recording) throws UnsupportedAudioFileException, IOException,
            InterruptedException, ExecutionException {
        try (AudioInputStream ais = AudioSystem.getAudioInputStream(recording)) {
            YinUnboundedStreamEstimator yse = new YinUnboundedStreamEstimator(ais, SETTINGS, 4);
            BlockingQueue<F0Candidate> estimates = yse.estimateUntilEOS();
            List<F0Candidate> collector = new LinkedList<>();
            while (true) {
                F0Candidate takenEstimate = estimates.take();
                if (takenEstimate.getTau() < 0) {
                    break;
                } else {
                    collector.add(takenEstimate);
                }
            }
            return collector;
        }
    }

}
