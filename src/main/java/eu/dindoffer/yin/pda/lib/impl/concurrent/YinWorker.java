package eu.dindoffer.yin.pda.lib.impl.concurrent;

import eu.dindoffer.yin.pda.core.api.F0Candidate;
import eu.dindoffer.yin.pda.core.impl.Yin;
import java.util.concurrent.Callable;

/**
 * A simple worker for one-time-use estimation.
 *
 * @author Martin Dindoffer 
 */
public class YinWorker implements Callable<F0Candidate> {

    private final Yin yin;
    private final int offset;

    /**
     * Creates a new YIN worker.
     *
     * @param yin    Yin algorithm instance to estimate upon
     * @param offset estimation offset, a.k.a. the probing frame number
     */
    public YinWorker(Yin yin, int offset) {
        this.yin = yin;
        this.offset = offset;
    }

    @Override
    public F0Candidate call() throws Exception {
        return yin.calculatePitch(offset);
    }
}
