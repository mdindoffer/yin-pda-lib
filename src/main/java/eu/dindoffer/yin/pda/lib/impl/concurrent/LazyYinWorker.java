package eu.dindoffer.yin.pda.lib.impl.concurrent;

import eu.dindoffer.yin.pda.core.api.F0Candidate;
import eu.dindoffer.yin.pda.core.api.YinSettings;
import eu.dindoffer.yin.pda.core.impl.Yin;
import java.util.concurrent.Callable;

/**
 * A one-time-use YIN worker able to obtain its own audio chunk and estimate it.
 *
 * @author Martin Dindoffer 
 */
public class LazyYinWorker implements Callable<F0Candidate> {

    private final YinSettings settings;
    private final long globalProbingSampleNum;
    private final int audioHistoryLength;
    private final int onwardsIntegrationWindowSize;
    private final LazyYinChunkFeeder lazyYinTaskFeeder;

    /**
     * Creates a new one-time-use YIN worker able to obtain its own audio chunk and estimate it.
     *
     * @param settings                     yin algorithm settings
     * @param globalProbingSampleNum       a frame position in the stream to use as estimation point
     * @param audioHistoryLength           the length of audio to required prepend to each estimation point
     * @param onwardsIntegrationWindowSize size of integration window from the estimation point onwards
     * @param lazyYinTaskFeeder            a feeder to use for obtaining audio chunks
     */
    public LazyYinWorker(YinSettings settings, long globalProbingSampleNum, int audioHistoryLength,
            int onwardsIntegrationWindowSize, LazyYinChunkFeeder lazyYinTaskFeeder) {
        this.settings = settings;
        this.globalProbingSampleNum = globalProbingSampleNum;
        this.lazyYinTaskFeeder = lazyYinTaskFeeder;
        this.audioHistoryLength = audioHistoryLength;
        this.onwardsIntegrationWindowSize = onwardsIntegrationWindowSize;
    }

    @Override
    public F0Candidate call() throws Exception {
        long decoderStartOffset = globalProbingSampleNum;
        boolean initialChunk = true;
        if (globalProbingSampleNum > audioHistoryLength) {
            decoderStartOffset -= audioHistoryLength;
            initialChunk = false;
        }
        long decoderEndOffsetExcl = globalProbingSampleNum + onwardsIntegrationWindowSize;
        Short[] frames = lazyYinTaskFeeder.getAnotherChunk(decoderStartOffset, decoderEndOffsetExcl);
        if (frames != null) {
            Yin yin = new Yin(Util.unBoxArray(frames), settings);
            return yin.calculatePitch(initialChunk ? 0 : audioHistoryLength);
        } else {
            return new F0Candidate(-1, -1);
        }
    }

}
