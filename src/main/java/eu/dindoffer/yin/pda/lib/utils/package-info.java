/**
 * Public utility classes intended for the consumers of the lib.
 */
package eu.dindoffer.yin.pda.lib.utils;
