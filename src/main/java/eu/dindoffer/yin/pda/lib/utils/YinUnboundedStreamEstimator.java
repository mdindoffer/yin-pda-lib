package eu.dindoffer.yin.pda.lib.utils;

import eu.dindoffer.yin.pda.core.api.F0Candidate;
import eu.dindoffer.yin.pda.core.api.YinSettings;
import eu.dindoffer.yin.pda.lib.impl.concurrent.InfiniteStreamCoordinator;
import eu.dindoffer.yin.pda.lib.impl.concurrent.InfiniteYinWorker;
import eu.dindoffer.yin.pda.lib.impl.concurrent.LazyYinChunkFeeder;
import eu.dindoffer.yin.pda.lib.impl.decode.AudioStreamDecoder;
import eu.dindoffer.yin.pda.lib.impl.decode.CachedAudioBuffer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * Utility class for estimation of pitches in unbounded PCM audio streams.
 *
 * @author Martin Dindoffer
 */
public class YinUnboundedStreamEstimator {

    private final AudioInputStream inputStream;
    private final YinSettings settings;
    private final ExecutorService threadPool;
    private final int parallelTasksCount;

    /**
     * Creates a new estimator for the given possibly unbounded audio input stream.
     * JEE users beware - this constructor creates its own threadpool.
     * For JEE environments use the {@link #YinUnboundedStreamEstimator(AudioInputStream, YinSettings, ExecutorService, int)}.
     *
     * @param inputStream        the audio to process. Estimator does not handle the stream lifecycle,
     *                           so be sure to close it appropriately
     * @param settings           settings of the Yin algorithm
     * @param parallelTasksCount number of parallel tasks to use for decoding and estimation
     */
    public YinUnboundedStreamEstimator(AudioInputStream inputStream, YinSettings settings, int parallelTasksCount) {
        this.inputStream = inputStream;
        this.settings = settings;
        this.parallelTasksCount = parallelTasksCount;
        this.threadPool = Executors.newFixedThreadPool(parallelTasksCount);
    }

    /**
     * Creates a new estimator for the given possibly unbounded audio input stream.
     *
     * @param inputStream        the audio to process. Estimator does not handle the stream lifecycle,
     *                           so be sure to close it appropriately
     * @param settings           settings of the Yin algorithm
     * @param executorService    executor service to use for both decoding and processing of the audio stream
     * @param parallelTasksCount number of parallel tasks to use for decoding and estimation
     */
    public YinUnboundedStreamEstimator(AudioInputStream inputStream, YinSettings settings,
            ExecutorService executorService, int parallelTasksCount) {
        this.inputStream = inputStream;
        this.settings = settings;
        this.threadPool = executorService;
        this.parallelTasksCount = parallelTasksCount;
    }

    /**
     * Simultaneously reads and estimates F0s of the audio stream until it reaches End of Stream.
     * <p>
     * The audio stream is read and processed in chunks consisting of 3 * tauMax frames.
     * <p>
     * This method does not block at all, both decoding and estimation occurs in separate threads.
     * Instead, a blocking queue is returned, upon which a caller may wait until the estimation finishes.
     * When an EOS is reached, an F0Candidate with period of -1 is written onto the queue to signal the end of estimation.
     * <p>
     * A default probing period of TauMax is used.
     *
     * @return a chronologically ordered blocking queue of results of the stream estimation (F0 period lengths
     *         in samples and their respective confidence values)
     * @throws UnsupportedAudioFileException if an unsupported audio stream is used,
     *                                       for further info see
     *                                       {@link AudioStreamDecoder#AudioStreamDecoder(AudioInputStream, int)}
     */
    public BlockingQueue<F0Candidate> estimateUntilEOS() throws UnsupportedAudioFileException {
        return estimateUntilEOS(settings.getTaumax());
    }

    /**
     * Simultaneously reads and estimates F0s of the audio stream until it reaches End of Stream.
     * <p>
     * The audio stream is read and processed in chunks consisting of 3 * tauMax frames.
     * <p>
     * This method does not block at all, both decoding and estimation occurs in separate threads.
     * Instead, a blocking queue is returned, upon which a caller may wait until the estimation finishes.
     * When an EOS is reached, an F0Candidate with period of -1 is written onto the queue to signal the end of estimation.
     *
     * @param probingPeriod a period, in which the estimation should take place (in samples)
     * @return a chronologically ordered blocking queue of results of the stream estimation (F0 period lengths
     *         in samples and their respective confidence values)
     * @throws UnsupportedAudioFileException if an unsupported audio stream is used,
     *                                       for further info see
     *                                       {@link AudioStreamDecoder#AudioStreamDecoder(AudioInputStream, int)}
     */
    public BlockingQueue<F0Candidate> estimateUntilEOS(int probingPeriod) throws UnsupportedAudioFileException {
        CachedAudioBuffer cab = new CachedAudioBuffer(inputStream, settings.getTaumax() * 2);
        int audioHistoryLength = settings.getTaumax() / 2;
        LazyYinChunkFeeder lazyYinTaskFeeder = new LazyYinChunkFeeder(probingPeriod, audioHistoryLength, cab);
        BlockingQueue<F0Candidate> results = new LinkedBlockingQueue<>();
        InfiniteStreamCoordinator infiniteStreamCoordinator = new InfiniteStreamCoordinator(results, probingPeriod);
        for (int i = 0; i < parallelTasksCount; i++) {
            InfiniteYinWorker infiniteYinWorker
                    = new InfiniteYinWorker(settings, lazyYinTaskFeeder, infiniteStreamCoordinator);
            threadPool.submit(infiniteYinWorker);
        }
        return results;
    }
}
