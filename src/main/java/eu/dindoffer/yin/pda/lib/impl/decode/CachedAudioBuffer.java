package eu.dindoffer.yin.pda.lib.impl.decode;

import java.util.ArrayList;
import java.util.List;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * An audio stream buffer.
 *
 * @author Martin Dindoffer 
 */
public class CachedAudioBuffer {

    private static final int DEFAULT_INIT_CACHE_SIZE = 441;//i.o.w. 10ms at 44.1khz

    private final List<Short> cache;
    private long firstCachedSampleNumber;
    private final DiscontinuousAudioStreamDecoder audioDecoder;

    /**
     * Creates a new buffer with default cache size of 441 frames (10ms at 44.1khz).
     *
     * @param ais input stream to buffer
     * @throws UnsupportedAudioFileException if the stream does not point
     *                                       to a valid audio file data recognized by the system.
     *                                       For further requirements see {@link AudioStreamDecoder#AudioStreamDecoder(AudioInputStream, int)}
     */
    public CachedAudioBuffer(AudioInputStream ais) throws UnsupportedAudioFileException {
        this(ais, DEFAULT_INIT_CACHE_SIZE);
    }

    /**
     * Creates a new buffer.
     *
     * @param ais              input stream to buffer
     * @param initialCacheSize the initial size of the buffer (in samples)
     * @throws UnsupportedAudioFileException if the stream does not point
     *                                       to a valid audio file data recognized by the system.
     *                                       For further requirements see {@link AudioStreamDecoder#AudioStreamDecoder(AudioInputStream, int)}
     */
    public CachedAudioBuffer(AudioInputStream ais, int initialCacheSize) throws UnsupportedAudioFileException {
        this.cache = new ArrayList<>(initialCacheSize);
        this.audioDecoder = new DiscontinuousAudioStreamDecoder(ais, 0);
    }

    /**
     * Retrieves specified decoded samples.
     * The samples positioned before the required starting position get dropped as they are now obsolete.
     *
     * @param fromSampleIncl sample number of the beginning of the audio bits to receive (inclusive)
     * @param toSampleExcl   sample number of the end of the audio bits to receive (exclusive)
     * @return an array of requested decoded samples
     */
    public Short[] getSamples(long fromSampleIncl, long toSampleExcl) {
        long cacheEndExclusive = firstCachedSampleNumber + cache.size();
        if (toSampleExcl > cacheEndExclusive) {
            if (!cacheSamples((int) (toSampleExcl - cacheEndExclusive))) {
                return null;
            }
        }
        int requestedStartingCacheIndexIncl = (int) (fromSampleIncl - firstCachedSampleNumber);
        int requestedEndingCacheIndexExcl = (int) (toSampleExcl - firstCachedSampleNumber);
        List<Short> requestedSamples = cache.subList(requestedStartingCacheIndexIncl, requestedEndingCacheIndexExcl);
        int outputSize = requestedEndingCacheIndexExcl - requestedStartingCacheIndexIncl;
        Short[] output = requestedSamples.toArray(new Short[outputSize]);
        dropOldSamples(requestedStartingCacheIndexIncl);
        firstCachedSampleNumber += requestedStartingCacheIndexIncl;
        return output;
    }

    private void dropOldSamples(int untilExclusive) {
        cache.subList(0, untilExclusive).clear();
    }

    private boolean cacheSamples(int framesToCache) {
        AudioChunk nextFrames = audioDecoder.nextFrames(framesToCache);
        if (nextFrames != null) {
            for (short s : nextFrames.getAudioBits()) {
                cache.add(s);
            }
            return true;
        } else {
            return false;
        }
    }

    public DiscontinuousAudioStreamDecoder getAudioDecoder() {
        return audioDecoder;
    }
}
