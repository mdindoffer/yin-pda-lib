package eu.dindoffer.yin.pda.lib.impl.concurrent;

/**
 *
 * @author Martin Dindoffer 
 */
public final class Util {

    private Util() {
    }

    public static short[] unBoxArray(Short[] array) {
        short[] result = new short[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }
}
