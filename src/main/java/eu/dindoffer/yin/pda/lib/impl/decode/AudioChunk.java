package eu.dindoffer.yin.pda.lib.impl.decode;

/**
 * A representation of a part of an audio stream.
 *
 * @author Martin Dindoffer 
 */
public class AudioChunk {

    private final boolean initialChunk;
    private final short[] audioBits;

    /**
     * Creates a new audio chunk.
     *
     * @param initialChunk whether this chunk represents the very first bytes in the audio stream
     * @param audioBits    audio data
     */
    public AudioChunk(boolean initialChunk, short[] audioBits) {
        this.initialChunk = initialChunk;
        this.audioBits = audioBits;
    }

    public boolean isInitialChunk() {
        return initialChunk;
    }

    public short[] getAudioBits() {
        return audioBits;
    }
}
