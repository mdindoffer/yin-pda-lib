package eu.dindoffer.yin.pda.lib.impl.concurrent;

import eu.dindoffer.yin.pda.lib.impl.decode.CachedAudioBuffer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * An on-demand retriever of chronologically ordered audio chunks in multithreaded environments from an audio buffer.
 *
 * @author Martin Dindoffer 
 */
public class LazyYinChunkFeeder {

    private final CachedAudioBuffer cab;
    private final int probingPeriod;
    private final int audioHistoryLength;
    private final Lock lock;
    private final Condition takeTurnCondition;
    private volatile long nextAwaitedSample;

    /**
     * Creates a new feeder of chronologically ordered audio chunks in multithreaded environments.
     *
     * @param probingPeriod      probing period (in samples) to use for estimation
     * @param audioHistoryLength the length of audio to required prepend to each estimation point
     * @param cab                a buffer to use for obtaining audio chunks
     */
    public LazyYinChunkFeeder(int probingPeriod, int audioHistoryLength, CachedAudioBuffer cab) {
        this.cab = cab;
        this.probingPeriod = probingPeriod;
        this.audioHistoryLength = audioHistoryLength;
        this.lock = new ReentrantLock(true);
        this.takeTurnCondition = lock.newCondition();
    }

    /**
     * Returns another chunk of audio samples between given sample-timestamps.
     * The method is thread safe, blocking all calling threads until it's their turn to decode data
     * (to preserve the order of input stream decoding in parallel execution)
     *
     * @param sampleFromNum starting sample number of the input stream to decode (inclusive)
     * @param sampleToNum   ending sample number of the input stream to decode (exclusive)
     * @return audio bits
     * @throws java.lang.InterruptedException if the thread waiting for its turn got interrupted
     */
    public Short[] getAnotherChunk(long sampleFromNum, long sampleToNum) throws InterruptedException {
        lock.lock();
        try {
            while (nextAwaitedSample != sampleFromNum) {
                takeTurnCondition.await();
            }
            Short[] frames = null;
            if (nextAwaitedSample == sampleFromNum) {
                int decodingOffsetIncrement = probingPeriod;
                if (nextAwaitedSample == 0) {
                    decodingOffsetIncrement -= audioHistoryLength;
                }
                nextAwaitedSample += decodingOffsetIncrement; //safe non-atomic operation, because the mutative access is exclusive
                frames = cab.getSamples(sampleFromNum, sampleToNum);
                takeTurnCondition.signalAll();
            }
            return frames;
        } finally {
            lock.unlock();
        }
    }
}
