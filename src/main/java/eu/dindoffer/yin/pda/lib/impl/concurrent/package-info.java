/**
 * Holds various helper classes used for concurrent audio decoding and estimation.
 */
package eu.dindoffer.yin.pda.lib.impl.concurrent;
