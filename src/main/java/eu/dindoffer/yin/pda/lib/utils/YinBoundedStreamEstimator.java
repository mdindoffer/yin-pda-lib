package eu.dindoffer.yin.pda.lib.utils;

import eu.dindoffer.yin.pda.core.api.F0Candidate;
import eu.dindoffer.yin.pda.core.api.YinSettings;
import eu.dindoffer.yin.pda.core.api.YinUtils;
import eu.dindoffer.yin.pda.lib.impl.decode.AudioChunk;
import eu.dindoffer.yin.pda.core.impl.Yin;
import eu.dindoffer.yin.pda.lib.impl.decode.AudioStreamDecoder;
import eu.dindoffer.yin.pda.lib.impl.decode.CachedAudioBuffer;
import eu.dindoffer.yin.pda.lib.impl.decode.DiscontinuousAudioStreamDecoder;
import eu.dindoffer.yin.pda.lib.impl.concurrent.LazyYinChunkFeeder;
import eu.dindoffer.yin.pda.lib.impl.concurrent.LazyYinWorker;
import eu.dindoffer.yin.pda.lib.impl.concurrent.YinWorker;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * Utility class for estimation of pitches in bounded PCM audio streams.
 *
 * @author Martin Dindoffer
 */
public class YinBoundedStreamEstimator {

    private final AudioInputStream inputStream;
    private final YinSettings settings;
    private final ExecutorService threadPool;

    /**
     * Creates a new estimator for the given audio input stream with specified framelength.
     * JEE users beware - this constructor creates its own threadpool.
     * For JEE environments use the {@link #YinBoundedStreamEstimator(AudioInputStream, YinSettings, ExecutorService)}.
     *
     * @param inputStream the audio to process. Estimator does not handle the stream lifecycle,
     *                    so be sure to close it appropriately
     * @param settings    settings of the Yin algorithm
     * @param threadCount number of threads to be used for stream estimation
     */
    public YinBoundedStreamEstimator(AudioInputStream inputStream, YinSettings settings, int threadCount) {
        this.inputStream = inputStream;
        this.settings = settings;
        this.threadPool = Executors.newFixedThreadPool(threadCount);
    }

    /**
     * Creates a new estimator for the given audio input stream with specified framelength.
     *
     * @param inputStream     the audio to process. Estimator does not handle the stream lifecycle,
     *                        so be sure to close it appropriately
     * @param settings        settings of the Yin algorithm
     * @param executorService executor service to use for both decoding and processing of the audio stream
     */
    public YinBoundedStreamEstimator(AudioInputStream inputStream, YinSettings settings, ExecutorService executorService) {
        this.inputStream = inputStream;
        this.settings = settings;
        this.threadPool = executorService;
    }

    /**
     * Reads all of the audio stream's frames and estimates the F0s with probing period of TauMax.
     * <p>
     * Entire stream (specified by its framelength attribute) gets decoded and loaded to memory at once,
     * which has several implications:
     * <ul>
     * <li>Higher memory consumption (i.e. a whole wav file decoded in memory)</li>
     * <li>Possibly less I/O operations</li>
     * <li>Possibly faster estimation, where memory is not an issue</li>
     * </ul>
     * <p>
     * This method does not block on F0 computation, but blocks during the stream's reading &amp; decoding.
     *
     * @return a list of future results of stream estimation (F0 period lengths in samples and their respective
     *         confidence values)
     * @throws IOException                   if an error occurs during the stream decoding
     * @throws UnsupportedAudioFileException if an unsupported audio stream is used,
     *                                       for further info see
     *                                       {@link AudioStreamDecoder#AudioStreamDecoder(AudioInputStream, int)}
     */
    public List<Future<F0Candidate>> estimateAllFramesEagerly()
            throws IOException, UnsupportedAudioFileException {
        return estimateAllFramesEagerly(settings.getTaumax());
    }

    /**
     * Reads all of the audio stream's frames and estimates the F0s with given probing period.
     * <p>
     * Entire stream (specified by its framelength attribute) gets decoded and loaded to memory at once,
     * which has several implications:
     * <ul>
     * <li>Higher memory consumption (i.e. a whole wav file decoded in memory)</li>
     * <li>Possibly less I/O operations</li>
     * <li>Possibly faster estimation, where memory is not an issue</li>
     * </ul>
     * <p>
     * This method does not block on F0 computation, but blocks during the stream's reading &amp; decoding.
     *
     * @param probingPeriod a period, in which the estimation should take place (in samples)
     * @return a list of future results of stream estimation (F0 period lengths in samples and their respective
     *         confidence values)
     * @throws IOException                   if an error occurs during the stream decoding
     * @throws UnsupportedAudioFileException if an unsupported audio stream is used,
     *                                       for further info see
     *                                       {@link AudioStreamDecoder#AudioStreamDecoder(AudioInputStream, int)}
     */
    public List<Future<F0Candidate>> estimateAllFramesEagerly(int probingPeriod)
            throws IOException, UnsupportedAudioFileException {
        try (DiscontinuousAudioStreamDecoder audioDecoder
                     = new DiscontinuousAudioStreamDecoder(inputStream, 0)) {
            AudioChunk entireAudioStream = audioDecoder.entireStream();
            Yin yin = new Yin(entireAudioStream.getAudioBits(), settings);
            List<YinWorker> chunkWorkers = new LinkedList<>();
            int minWindow = YinUtils.calculateMinimalIntegrationWindowSize(probingPeriod);
            long lastWindowPosition = audioDecoder.getFrameLength() - minWindow;
            int offset = -probingPeriod;
            do {
                offset += probingPeriod;
                chunkWorkers.add(new YinWorker(yin, offset));
            } while (offset < lastWindowPosition);
            List<Future<F0Candidate>> results = new ArrayList<>();
            for (Callable<F0Candidate> task : chunkWorkers) {
                Future<F0Candidate> taskFuture = threadPool.submit(task);
                results.add(taskFuture);
            }
            return results;
        }
    }

    /**
     * Simultaneously reads and estimates F0s of the entire audio stream with a probing period of TauMax.
     * <p>
     * The audio stream is read and processed lazily, in chunks consisting of 3 * tauMax frames.
     * The decoding process consumes more CPU/bandwidth resources than its eager counterparts.
     * However, this lazy estimation has lower memory footprint, making it useful for huge audio streams.
     *
     * @return a list of future results of stream estimation (F0 period lengths in samples and their respective
     *         confidence values)
     * @throws UnsupportedAudioFileException if an unsupported audio stream is used,
     *                                       for further info see
     *                                       {@link AudioStreamDecoder#AudioStreamDecoder(AudioInputStream, int)}
     */
    public List<Future<F0Candidate>> estimateAvailableDataLazily() throws UnsupportedAudioFileException {
        return estimateAvailableDataLazily(settings.getTaumax());
    }

    /**
     * Simultaneously reads and estimates F0s of the entire audio stream with given probing period.
     * <p>
     * The audio stream is read and processed lazily, in chunks consisting of 3 * tauMax frames.
     * The decoding process consumes more CPU/bandwidth resources than its eager counterparts.
     * However, this lazy estimation has lower memory footprint, making it useful for huge audio streams.
     *
     * @param probingPeriod a period, in which the estimation should take place (in samples)
     * @return a list of future results of stream estimation (F0 period lengths in samples and their respective
     *         confidence values)
     * @throws UnsupportedAudioFileException if an unsupported audio stream is used,
     *                                       for further info see
     *                                       {@link AudioStreamDecoder#AudioStreamDecoder(AudioInputStream, int)}
     */
    public List<Future<F0Candidate>> estimateAvailableDataLazily(int probingPeriod) throws UnsupportedAudioFileException {
        CachedAudioBuffer cab = new CachedAudioBuffer(inputStream, settings.getTaumax() * 2);
        int audioHistoryLength = settings.getTaumax() / 2;
        LazyYinChunkFeeder lazyYinTaskFeeder = new LazyYinChunkFeeder(probingPeriod, audioHistoryLength, cab);
        List<LazyYinWorker> chunkWorkers = new LinkedList<>();
        int minWindow = YinUtils.calculateMinimalIntegrationWindowSize(probingPeriod);
        long lastWindowPosition = cab.getAudioDecoder().getFrameLength() - minWindow;
        int chunkNumber = -1;
        int offset;
        do {
            chunkNumber++;
            offset = chunkNumber * probingPeriod;
            chunkWorkers.add(new LazyYinWorker(
                    settings, chunkNumber * (long) probingPeriod, audioHistoryLength, minWindow, lazyYinTaskFeeder));
        } while (offset < lastWindowPosition);
        List<Future<F0Candidate>> results = new ArrayList<>(chunkNumber + 1);
        for (Callable<F0Candidate> task : chunkWorkers) {
            Future<F0Candidate> resultFuture = threadPool.submit(task);
            results.add(resultFuture);
        }
        return results;
    }
}
