package eu.dindoffer.yin.pda.lib.impl.decode;

import java.io.IOException;
import java.io.InputStream;
import java.util.NoSuchElementException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * An audio stream decoder which decodes the stream in separate, non-overlapping chunks.
 *
 * @author Martin Dindoffer 
 */
public class DiscontinuousAudioStreamDecoder extends AudioStreamDecoder {

    public DiscontinuousAudioStreamDecoder(InputStream inputStream, int defaultChunkSize)
            throws IOException, UnsupportedAudioFileException {
        super(inputStream, defaultChunkSize);
    }

    public DiscontinuousAudioStreamDecoder(AudioInputStream audioInputStream, int defaultChunkSize)
            throws UnsupportedAudioFileException {
        super(audioInputStream, defaultChunkSize);
    }

    /**
     * Decodes the entire stream.
     *
     * @return decoded stream
     * @throws IOException if an i/o error occurs while reading the input stream
     */
    public AudioChunk entireStream() throws IOException {
        return nextChunk((int) getFrameLength());
    }

    public AudioChunk nextFrames(int framesToDecode) {
        try {
            return super.nextChunk(framesToDecode);
        } catch (IOException ex) {
            throw new NoSuchElementException();
        }
    }

    @Override
    public AudioChunk next() {
        return nextFrames(defaultChunkSize);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("remove");
    }
}
