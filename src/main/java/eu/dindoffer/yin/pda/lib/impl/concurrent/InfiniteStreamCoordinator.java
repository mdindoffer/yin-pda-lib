package eu.dindoffer.yin.pda.lib.impl.concurrent;

import eu.dindoffer.yin.pda.core.api.F0Candidate;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Coordinates the parallel work of infinite workers.
 *
 * @author Martin Dindoffer 
 */
public class InfiniteStreamCoordinator {

    private final int probingPeriod;
    private final AtomicLong nextProbingFramePosition;
    private final BlockingQueue<F0Candidate> resultSink;
    private final Lock lock;
    private final Condition takeTurnCondition;
    private volatile long nextAwaitedResultPosition;

    /**
     * Creates a new coordinator of parallel unbounded workers.
     *
     * @param resultSink    output queue to use for publishing chronologically ordered results
     * @param probingPeriod a period, in which the estimation should take place (in samples)
     */
    public InfiniteStreamCoordinator(BlockingQueue<F0Candidate> resultSink, int probingPeriod) {
        this.probingPeriod = probingPeriod;
        this.nextProbingFramePosition = new AtomicLong();
        this.nextAwaitedResultPosition = 0;
        this.resultSink = resultSink;
        this.lock = new ReentrantLock(true);
        this.takeTurnCondition = lock.newCondition();
    }

    /**
     * Generates next frame position number to use as estimation point.
     *
     * @return next probing frame position number
     */
    public long getNextProbingFramePosition() {
        return nextProbingFramePosition.getAndAdd(probingPeriod);
    }

    /**
     * Publishes an estimation result at given probing frame position.
     * The method is thread safe, blocking all calling threads until it's their turn to publish data
     * (to preserve chronological ordering in the output queue).
     *
     * @param position position at which the estimation took place
     * @param result   estimation result (in samples)
     * @throws InterruptedException when the calling thread got interrupted while waiting for its turn,
     *                              or while pushing the result to the output queue
     */
    public void publishResult(long position, F0Candidate result) throws InterruptedException {
        lock.lock();
        try {
            while (nextAwaitedResultPosition != position) {
                takeTurnCondition.await();
            }
            if (nextAwaitedResultPosition == position) {
                nextAwaitedResultPosition += probingPeriod; //safe non-atomic operation, because the mutative access is exclusive
                resultSink.put(result);
                takeTurnCondition.signalAll();
            }
        } finally {
            lock.unlock();
        }
    }
}
