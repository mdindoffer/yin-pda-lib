# YIN Pitch Detection Algorithm Library #

The YIN pda library for Java wraps the YIN algorithm with utilities for easy estimation of PCM AudioInputStreams.
For the actual YIN implementation see [yin-pda-core](https://bitbucket.org/mdindoffer/yin-pda-core).

#Usage

The library takes care of both decoding the PCM audio streams and estimation. The entire process is parallelized.

Apart from standard YIN parameters (threshold, neighborhood deviation, tauMax), one can specify also the estimation
probing period. It represents the time interval in samples with which the audio stream's F0 is estimated. In other
words a time resolution of results.

##Bounded Streams

Bounded streams are audio streams with specified frame length. For example created from a wav file.
The associated estimator class is YinBoundedStreamEstimator with a couple of utility methods which return a list
of futures containing F0 estimates in chronological order. E.g.:

```java
try (AudioInputStream ais = AudioSystem.getAudioInputStream(file)) { //get audio stream from a file
    YinBoundedStreamEstimator yse = new YinBoundedStreamEstimator(ais, settings, 4); // inject desired settings, use 4 threads
    List<Future<F0Candidate>> estimates = yse.estimateAvailableDataLazily(); // estimate with default probing period
    List<F0Candidate> results = new ArrayList<>(estimates.size());
    for (Future<F0Candidate> f : estimates) {
        results.add(ais.getFormat().getSampleRate() / f.get());//convert period length to F0 frequency
    }
}
```

##Unbounded Streams

The lib also supports estimation of unbounded streams - i.e. real-time streams with non-specified frame length.

```java
YinUnboundedStreamEstimator yse = new YinUnboundedStreamEstimator(ais, settings, 4);
BlockingQueue<F0Candidate> estimates = yse.estimateUntilEOS(settings.getTaumax());
while (true) {
    F0Candidate takenEstimate = estimates.take();
    if (takenEstimate.getTau() < 0) {
        break;
    } else {
        double result = ais.getFormat().getSampleRate() / takenEstimate.getTau();
        System.out.println("Estimated frequency: " + result);
    }
}
```

##Known Limitations

* Accepts only 16b audio

#Platform

* Requires at least Java 7
* OSGi compatible
* JavaEE friendly

#Maven
```xml
<dependency>
    <groupId>eu.dindoffer</groupId>
    <artifactId>yin-pda-lib</artifactId>
    <version>1.1.1</version>
</dependency>
```

#License
Licensed under the MIT license.

#Author & Contact
I (Martin Dindoffer) created this library in my precious free time just for fun.
If you like it, have a feature request, or just want to buy me a beer, drop me an email at contact_ta_dindoffer.eu